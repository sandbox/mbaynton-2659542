<?php
/**
 * Created by PhpStorm.
 * User: mike
 * Date: 12/12/15
 * Time: 10:35 AM
 */

namespace Drupal\malt;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

interface MarkupAreaInterface extends ConfigEntityInterface {

}
