<?php

/**
 * @file
 * Contains \Drupal\malt\Entity\MarkupArea
 */

namespace Drupal\malt\Entity;

use Drupal\malt\MarkupAreaInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the MarkupArea Entity.
 *
 * @configEntityType(
 *   id = "malt_markup_area",
 *   label = @Translation("Markup Area"),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\malt\Form\MarkupAreaForm",
 *       "edit" = "Drupal\malt\Form\MarkupAreaForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     },
 *     "list_builder" = "Drupal\malt\MarkupAreaListBuilder",
 *   },
 *   admin_permission = "administer malts",
 *   config_prefix = "markup_area",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   links = {
 *     "edit-form" = "/admin/config/content/malt/markup_area/manage/{malt_markup_area}",
 *     "delete-form" = "/admin/config/content/malt/markup_area/manage/{malt_markup_area}/delete",
 *     "collection" = "/admin/config/content/malt",
 *   },
 * )
 */
class MarkupArea extends ConfigEntityBase implements MarkupAreaInterface {
  public $id;

  public $label;

  public $format;

  public $markup;
}
