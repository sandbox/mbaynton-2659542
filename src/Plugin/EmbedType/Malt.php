<?php

namespace Drupal\malt\Plugin\EmbedType;

use Drupal\embed\EmbedType\EmbedTypeBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Malt embed type.
 *
 * @EmbedType(
 *   id = "malt",
 *   label = @Translation("Malt Flavor")
 * )
 */
class Malt extends EmbedTypeBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultIconUrl() {
    return file_create_url(drupal_get_path('module', 'malt') . '/js/plugins/drupalmalt/malt.png');
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    // whatever we decide is needed for a malt flavor's config
    return [
      'thing' => null,
    ];
  }

  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $table = array(
      '#type' => 'table',
      '#header' => array($this->t('Area'), $this->t('Editable'), $this->t('Configure')),
      '#attributes' => array(
        'id' => 'malt-areas-table',
      ),
      '#regions' => array(
        'title' => $this->t('Markup Areas'),
        'message' => $this->t('No areas are defined.')
      ),
      '#tabledrag' => array(
        array(
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'area-weight',
        ),
      ),
    );

    array_merge($table, $this->getAreaRows());

    $form['markup_area_table'] = $table;

    return $form;
  }

  protected function getAreaRows() {
    $test_data = ['abc', 'def', 'ghi'];
    $rows = [];

    foreach ($test_data as $label) {
      $rows[$label] = array(
        '#attributes' => array('class' => array('draggable', 'tabledrag-leaf')),
        'human_name' => array(
          '#plain_text' => $label,
        ),
        'weight' => array(
          '#type' => 'textfield',
          '#title' => $this->t('Weight for @title', array('@title' => $label)),
          '#title_display' => 'invisible',
          '#default_value' => '0',
          '#size' => 3,
          '#attributes' => array('class' => array('field-weight')),
        ),
        'parent_wrapper' => array(
          'parent' => array(
            '#type' => 'select',
            '#title' => $this->t('Label display for @title', array('@title' => $label)),
            '#title_display' => 'invisible',
            '#options' => array(),
            '#empty_value' => '',
            '#attributes' => array('class' => array('js-field-parent', 'field-parent')),
            '#parents' => array('fields', $field_name, 'parent'),
          ),
          'hidden_name' => array(
            '#type' => 'hidden',
            '#default_value' => $field_name,
            '#attributes' => array('class' => array('field-name')),
          ),
        ),
      );

      $rows[$label] += array(
        '#row_type' => 'field',
        '#region_callback' => array($this, 'getRowRegion'),
      );
    }

    return $rows;
  }

}
